%{
	int identifier=0, number=0, keyword=0, assign=0, relop=0, unioperator=0, bioperator=0, special=0, filecount=0, curfile=1;
	char** args;
%}

identifier	[a-zA-Z_][a-zA-Z0-9_]*
number	[-+]?[0-9]+(\.[0-9]+)?([eE][-+]?[0-9]+)?
space	[ \t\n]
assign	=
relop	(<|<=|>|>=|<>|==)
unioperator	("++"|"--"){identifier}
bioperator	[+-/\*]
keyword	(int|float|return|print|if|def|while)[\t ]
openkeywords	(else)[\t ]
special	[():]
%%
{number}    {
        printf("Number: %s\n", yytext);
        number++;
}
{assign}    {
        printf("Assignment: %s\n", yytext);
        assign++;
}
{relop}    {
        printf("Relational Operator: %s\n", yytext);
        relop++;
}
{unioperator}    {
	yyless(2);
        printf("Unary Operator: %s\n", yytext);
        unioperator++;
}
{bioperator}    {
        printf("Binary Operator: %s\n", yytext);
        bioperator++;
}
{keyword}	{
	printf("Keyword: %s\n", yytext);
	keyword++;
}
{openkeywords}       {
	yymore();
        printf("Keyword: %s\n", yytext);
        keyword++;
}

{identifier}    {
        printf("Identifier: %s\n", yytext);
        identifier++;
}
{special}    {
        printf("Special: %s\n", yytext);
        special++;
}

.	
%%

int yywrap() {
	if(curfile!=filecount) {
		curfile++;
		yyin=fopen(args[curfile], "r");
		return 0;
	}
	return 1;
}

int main(int argc, char* argv[]) {
	filecount=argc-1;
	//for(int i=1; i<argc; i++) {
	args = argv;
		yyin = fopen(argv[curfile], "r");
		printf("File: %s\n", argv[curfile]);
		yylex();
		printf("\nCount:\n");
		printf("Identifiers: %d\n", identifier);
		printf("Number: %d\n", number);
		printf("Keyword: %d\n", keyword);
		printf("Relational Operator: %d\n", relop);
		printf("Unary Operator: %d\n", unioperator);
		printf("Binary Operator: %d\n", bioperator);
		printf("Assignment: %d\n", assign);
		printf("Special: %d\n", special);
		printf("--------------xxxxxxxxxxxxxxxxxx----------------\n");
		identifier=0;number=0;keyword=0;relop=0;unioperator=0;bioperator=0;assign=0, special=0;
	//}
	return 0;
}

