infix	^[a-zA-z].*[a-zA-Z]"\n"
operator	[+-/\*=]
prefix	^{operator}.*[a-zA-Z]"\n"
postfix	^[a-zA-Z].*{operator}"\n"

%%
{prefix} {
	printf("Prefix: %s\n", yytext);
}
{postfix} {
        printf("Postfix: %s\n", yytext);
}
{infix} {
        printf("Infix: %s\n", yytext);
}
%%

int main(int argc, char* argv[]) {
        for(int i=1; i<argc; i++) {
                yyin = fopen(argv[i], "r");
                printf("File: %s\n", argv[i]);
		yylex();
	}
	return 0;
}
