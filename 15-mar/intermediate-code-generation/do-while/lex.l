ALPHA [A-Za-z]
DIGIT [0-9]
%%
while                return WHILE;
do  return DO;
{ALPHA}({ALPHA}|{DIGIT})*    return ID;
{DIGIT}+             {yylval=atoi(yytext); return NUM;}
[ \t\n]                 ;
.                 return yytext[0];
%%