%token ID INTNUM FLOATNUM OP END TYPE
%left '+' '-'
%left '*' '/'
%{
	#include <stdio.h>
	#include <string.h>
        #include<vector>
        using namespace std;
        extern "C" {
	        int yylex(void);
                int yywrap(void) {
                        return 1;
                }
                int yyparse(void);
        }
        void yyerror(char*);
	void insert(char* type, char* name);
        char* lookup(char* name);
        vector< pair<char*, char*> > symtable;
	char* curtype;
%}

%union {
	char* type;
	int intvalue;
	float floatvalue;
	char* idname;
}

%%
program:
	program statement '\n' {}
	|
	;
statement:
	TYPE {
		curtype = (char*) malloc(sizeof($<type>1));
                strcpy(curtype, $<type>1);
	} dlist END
	| ID '=' ID END	{
		if(lookup($<idname>1) == NULL) {
			printf("%s Undeclared\n", $<idname>1);
		} else if(lookup($<idname>3) == NULL) {
			printf("%s Undeclared\n", $<idname>3);
		} else if(strcmp(lookup($<idname>1), lookup($<idname>3)) != 0) {
			if(strcmp(lookup($<idname>1), "int") == 0)	{
				printf("Error: Cannot cast float(%s) to int\n", $<idname>3);
			} else {
				printf("Casting int(%s) to float\n", $<idname>3);
			}
		}
	}
	| ID '=' FLOATNUM END {
		if(lookup($<idname>1) == NULL) {
                        printf("%s Undeclared\n", $<idname>1);
                } else {
                        if(strcmp(lookup($<idname>1), "int") == 0)      {
                                printf("Error: Cannot cast float(%f) to int\n", $<floatvalue>3);
			}
                }
	}
	| ID '=' INTNUM END {
                if(lookup($<idname>1) == NULL) {
                        printf("%s Undeclared\n", $<idname>1);
                } else {
                        if(strcmp(lookup($<idname>1), "float") == 0)      {
                                printf("Casting int(%d) to float\n", $<intvalue>3);
                        }
                }
        }
	;

dlist:
	dlist ',' ID	{
		//printf("Inserting %s(%s) into symbol table\n", curtype, $<idname>3);
                insert(curtype, $<idname>3);
	}
	| ID	{
		//printf("Inserting %s(%s) into symbol table\n", curtype, $<idname>1);
		insert(curtype, $<idname>1);
	}
	;
%%

void insert(char* type, char* name) {
        symtable.push_back(make_pair(type, name));
}

char* lookup(char* name) {
        for(vector< pair<char*, char*> >::iterator it = symtable.begin(); it!=symtable.end(); ++it) {
                if(strcmp(it->second, name) == 0) {
                        return it->first;
                }
        }
        return NULL; 
}


void yyerror(char *s) {
	printf("%s\n", s);
}

int main(void) {
	yyparse();
	printf("--Symbol table--\n");
	printf("Type\tName\n");
	for(vector< pair<char*, char*> >::iterator it = symtable.begin(); it!=symtable.end(); ++it) {
                printf("%s\t%s\n", it->first, it->second);
        }
	return 0;
}
