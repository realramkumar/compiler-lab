%{
	int digitcount=0, uppercount=0, lowercount=0;
%}
digit [0-9]
upper [A-Z]
lower [a-z]
%%
{digit}	digitcount++;
{upper}	uppercount++;
{lower}	lowercount++;
[.\t\n ]+
%%
int main() {
	yylex();
	printf("There were %d digits, %d upper characters in %d lower characters\n", digitcount, uppercount, lowercount);
	return 0;
}
