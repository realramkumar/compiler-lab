%{
	int charcount=0, linecount=0, wordcount=0;
%}
%%
\n {
	linecount++; 
	charcount++;
	wordcount++;
}
\t {
	charcount++;
	wordcount++;
}
" " 	wordcount++;
. 	charcount++;
%%
int main() {
	yylex();
	printf("There were %d characters and %d words in %d lines\n", charcount, wordcount, linecount);
	return 0;
}
