%{
	int emailcounter=0;
%}
email (^|[ \t])[a-z0-9]+@[a-z]+\.[a-z]+([ \t\n]|$)
%%
{email}	emailcounter++;
.	
%%
int main() {
	yylex();
	printf("%d emails found\n", emailcounter);
	return 0;
}
