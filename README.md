# Compiling the Code

## For only lex programs
````
lex filename.l
cc lex.yy.c -ll
````

## For programs using yacc
````
lex filename.l
yacc -d filename.y
cc lex.yy.c y.tab.c -ll
````
