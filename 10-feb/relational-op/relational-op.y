%token INTEGER VARIABLE GT GTE LT LTE EQ NEQ
%left '+' '-'
%left '*' '/'
%{
	#include <stdio.h>
	void yyerror(char *);
	int yylex(void);
	int sym[26];
%}
%%
program:
	program statement '\n'
	|
	;
statement:
	expr	{ printf("%d\n", $1); }
	| VARIABLE '=' expr	{ sym[$1] = $3; }
	;

expr:
	INTEGER	{ $$ = $1; }
	| expr LT expr	{ $$ = ($1<$3)?1:0; }
	| expr GT expr	{ $$ = ($1>$3)?1:0; }
	| expr LTE expr  { $$ = ($1<=$3)?1:0; }
        | expr GTE expr  { $$ = ($1>=$3)?1:0; }
	| expr EQ expr	{ $$ = ($1==$3)?1:0; }
	| expr NEQ expr	{ $$ = ($1==$3)?0:1; }
	| '(' expr ')'	{ $$ = $2; }
	;
%%
void yyerror(char *s) {
	printf("%s\n", s);
}
int main(void) {
	yyparse();
	return 0;
}
