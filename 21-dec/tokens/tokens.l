%{
	int identifier=0, number=0, whitespace=0, assign=0, operator=0, keyword=0;
%}

identifier	[a-zA-Z_][a-zA-Z0-9_]*
number	[-+]?[0-9]+(\.[0-9]+)?([eE][-+]?[0-9]+)?
space	[ \t\n]
assign	:=
operator	[+-/\*]
keyword	(int|float|if|while|else)[\t ]

%%
{number}    {
        printf("Number: %s\n", yytext);
        number++;
}
{space}    {
        printf("Whitespace: %s\n", yytext);
        whitespace++;
}
{assign}    {
        printf("Assignment: %s\n", yytext);
        assign++;
}
{operator}    {
        printf("Operator: %s\n", yytext);
        operator++;
}
{keyword}    {
        printf("Keyword: %s\n", yytext);
        keyword++;
}

{identifier}    {
        printf("Identifier: %s\n", yytext);
        identifier++;
}
.	
%%

int main() {
	yylex();
	printf("\nCount:\n");
	printf("Identifiers: %d\n", identifier);
	printf("Number: %d\n", number);
	printf("Whitespace: %d\n", whitespace);
	printf("Operator: %d\n", operator);
	printf("Assignment: %d\n", assign);
	printf("Keywords:%d\n", keyword);
	return 0;
}

