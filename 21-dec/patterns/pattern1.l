%{
void p(char* str) {
        printf("For pattern %s: %s\n", str, yytext);
}
%}

%%
abc	p("abc");
d.*t	p("d to t");
(ab)+	p("ab repeat");
[+-]?[0-9]+	p("digits");
[\n\t]	
.		
%%

int main() {
	yylex();
	return 0;
}
