%{
	#include<stdlib.h>
	#include "y.tab.h"
%}

digit	[0-9]
letter	[A-Za-z]
id	{letter}({letter}|{digit})*
operator	[-+*/]

%%
{id}	{
	yylval = *yytext;
	return VARIABLE;
}
[ \t\n]	
.	
%%

