%{
	#include <stdio.h>
	int yylex(void);
	void yyerror(char *);
	#define YYSTYPE char*
%}

%token NUMBER VARIABLE OPERATOR

%%
program:
	program expr '\n'
	|
	;

expr:
	NUMBER
	| VARIABLE
	| expr OPERATOR expr	{printf("%s\n", $$);}
	|
	;
%%

void yyerror(char *s) {
	printf("%s\n", s);
}

int main(void) {
	yyparse();
	return 0;
}
