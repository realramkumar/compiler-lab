%{
	int identifier=0, number=0, whitespace=0, assign=0, operator=0;
%}

identifier	[a-zA-Z_][a-zA-Z0-9_]*
number	[-+]?[0-9]+(\.[0-9]+)?([eE][-+]?[0-9]+)?
space	[ \t\n]
assign	:=
operator	[+-/\*]
keyword	(int|float|if|while|else)[\t ]

%%
{number}    {
        printf("Number: %s\n", yytext);
        number++;
}
{space}    {
        printf("Whitespace: %s\n", yytext);
        whitespace++;
}
{assign}    {
        printf("Assignment: %s\n", yytext);
        assign++;
}
{operator}    {
        printf("Operator: %s\n", yytext);
        operator++;
}
{identifier}    {
        printf("Identifier: %s\n", yytext);
        identifier++;
}
.	
%%

int main(int argc, char* argv[]) {
	for(int i=1; i<argc; i++) {
		yyin = fopen(argv[i], "r");
		printf("File: %s\n", argv[i]);
		yylex();
		printf("\nCount:\n");
		printf("Identifiers: %d\n", identifier);
		printf("Number: %d\n", number);
		printf("Whitespace: %d\n", whitespace);
		printf("Operator: %d\n", operator);
		printf("Assignment: %d\n", assign);
		printf("--------------xxxxxxxxxxxxxxxxxx----------------\n");
		identifier=0;number=0;whitespace=0;operator=0;assign=0;
	}
	return 0;
}

