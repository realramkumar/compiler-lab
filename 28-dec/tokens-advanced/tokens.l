%{
	int relop=0, str=0, spc=0, constant=0, keyword=0;
%}

linecom	"//".*\n
mulcom	"/*"(.|\n)*(\"(.|\n)*"*/"(.|\n)*\")?(.|\n)*"*/"
relop	(<|<=|>|>=|<>|==)
cons	[-+]?[0-9]+(\.[0-9]+)?([E][-+]?[0-9]+)?
spc	[!@#$%^&*()]
str	\"[a-zA-Z]+\"[ \t]
keyword	(int|float|real|if|then|for|while|else)[\t ]

%%
{linecom}	
{mulcom}	
{relop}    {
        printf("Relational: %s\n", yytext);
        relop++;
}
{cons}    {
        printf("Constant: %s\n", yytext);
        constant++;
}
{spc}    {
        printf("Special chars: %s\n", yytext);
        spc++;
}
{str}    {
        printf("String: %s\n", yytext);
        str++;
}
{keyword}    {
        printf("Keyword: %s\n", yytext);
        keyword++;
}
.	
%%

int main(int argc, char* argv[]) {
	for(int i=1; i<argc; i++) {
		yyin = fopen(argv[i], "r");
		printf("File: %s\n", argv[i]);
		yylex();
		printf("\nCount:\n");
		printf("Relational: %d\n", relop);
		printf("Constant: %d\n", constant);
		printf("Special Chars: %d\n", spc);
		printf("Strings: %d\n", str);
		printf("Keywords:%d\n", keyword);
		relop=0;constant=0;spc=0;str=0;keyword=0;
		printf("--------------xxxxxxxxxxxxxxxxxx----------------\n");
	}
	return 0;
}

