%{
  #include<stdio.h>
  #include<stdlib.h>
  void RemoveSpaces(char*);
  char *toks, *init, *cond, *itr, *body;
%}

num [-+]?[0-9]+(\.[0-9]+)?([eE][-+]?[0-9]+)?
id  [a-zA-Z_][a-zA-Z0-9_]*
relop (>|<|<=|>=|==)
/* [ ]* to handle optional spaces between the line */
init  {id}[ ]*"="[ ]*({num}|{id})
cond  ({id}|{num})[ ]*{relop}[ ]*({id}|{num})
itr (("++"|"--"){id}|{id}("++"|"--"))
body  "{"(.|\n)*"}"

%%
"for"[ ]*"("[ ]*{init}[ ]*";"[ ]*{cond}[ ]*";"[ ]*{itr}[ ]*")"  {
  RemoveSpaces(yytext); //Remove the optional spaces.

  //Move toks pointer to open brace
  toks = strchr(yytext, '(');
  char* p;

  //Returns till first ;
  p = strtok(toks+1, ";");
  init = (char*) malloc(sizeof(p));
  strcpy(init, p);

  //Returns till next ;
  p = strtok(NULL, ";");
  cond = (char*) malloc(sizeof(p));
  strcpy(cond, p);

  //Returns till next ;
  p = strtok(NULL, ")");
  itr = (char*) malloc(sizeof(p));
  strcpy(itr, p);
}

{body}  {
  body = (char*) malloc(sizeof(yytext));
  strcpy(body, yytext+1); //leave curly brace
  //Overwrite last curly brace with \0
  strcpy(strrchr(body, '}'), "\0");
}

[.\n\t ]  

%%

void RemoveSpaces(char* source)
{
  char* i = source;
  char* j = source;
  while(*j != 0)
  {
    *i = *j++;
    if(*i != ' ')
      i++;
  }
  *i = 0;
}

int yywrap(void) {
  return 1;
}

int main() {
  yylex();
  printf("%s;\n", init);
  printf("while(%s){", cond);
  printf("%s\n", body);
  printf("%s;\n}\n", itr);
  return 0;
}