let [a-zA-Z]
dig [0-9]
incc	(({let}+|{dig}+)"++"|({let}+|{dig})+"--")
%{
#include<stdio.h>
char init[10],cond[10],inc[5], body[50];
%}
%%
"while" {printf("for");}
[\(] {printf("%s",yytext);}
({let}+|{dig}+)"="({let}+|{dig}+); { strcpy(init,yytext); }
({let}+|{dig}+)[<=>]*({let}+|{dig}+) { strcpy(cond,yytext); }
{incc} { strcpy(inc,yytext); }
. {}
[ \n\t] {}
%%
int main()
{
yylex();
printf("%s%s;%s) {\n}\n",init,cond,inc);
return 0;
}



