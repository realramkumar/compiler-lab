%{
#include<stdio.h>
#include<string.h>
char buf[100];
%}

%union
{
	char *name;
}
%token<name>NUM ID
%type<name>E
%left'+''-'
%left'*''/'
%nonassoc UMINUS
%%
S:E	{printf("\n%s\n", $1);}
;
E:E'*'E	{buf[0]='\0';strcpy($$, strcat(strcat($1,$3), strcpy(buf, "*")));}
 |E'/'E	{buf[0]='\0';strcpy($$, strcat(strcat($1,$3), strcpy(buf, "/")));}
 |E'+'E	{buf[0]='\0';strcpy($$, strcat(strcat($1,$3), strcpy(buf, "+")));}
 |E'-'E	{buf[0]='\0';strcpy($$, strcat(strcat($1,$3), strcpy(buf, "-")));}
 |ID
 |NUM
 |'('E')'	{$$=(char*)malloc(strlen($2)+1);strcpy($$,$2);}
;
%%
int main() {
	yyparse();
	return 0;
}
int yyerror(char *s) {
	fprintf(stderr, "\n%s", s);
}
