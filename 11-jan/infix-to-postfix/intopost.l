%{
#include<string.h>
#include"intopost.tab.h"
%}
%%
"*"|"/"|"+"|"-"|"("|")"	{
	return yytext[0];
}
[0-9]+	{
	yylval.name=(char*)malloc(yyleng+1);
	strcpy(yylval.name, yytext);
	return NUM;
}
\n	{
	yywrap();
}
[a-zA-Z]+	{
	yylval.name=(char*)malloc(yyleng+1);
	strcpy(yylval.name, yytext);
	return ID;
}
.	{}

%%
int yywrap() {
        if(curfile!=filecount) {
                curfile++;
                yyin=fopen(args[curfile], "r");
                return 0;
        }
        return 1;
}

