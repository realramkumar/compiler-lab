%{
	#include<stdio.h>
	void yyerror(char *s);
	int yylex(void);
%}

%token NUM ID FOR WHILE IF ELSE END OP RELOP ITR LOGOP

%%
S	:LOOPS
	|IFELSES
	;

LOOPS	:LOOPS LOOP
	|
	;

LOOP	:HEAD BODY	{printf("Valid\n");}
	|HEAD END	{printf("Valid\n");}
	;

HEAD	:FOR '(' E END C END I ')'
	|WHILE '(' C ')'
	;

IFELSES	:IFELSES IFELSE
	|
	;

IFELSE	:IF '(' C ')' BODY ELSEIFS ELSE BODY	{printf("Valid\n");}
	|IF '(' C ')' BODY ELSEIFS	{printf("Valid\n");}
	|IF '(' C ')' BODY	{printf("Valid\n");}
	|IF '(' C ')' END	{printf("Valid\n");}
	;

ELSEIFS	:ELSEIFS ELSE IF '(' C ')' BODY
	|
	;

BODY	:'{' STATEMENTS '}'
	;
STATEMENTS	:STATEMENTS E END
		|STATEMENTS I END
		|STATEMENTS LOOP
		|STATEMENTS IFELSE
		|
		;
E	:E OP E
	|ID
	|NUM
	;
C	:E RELOP E
	|E
	|C LOGOP C
	|'(' C ')'
	;
I	:E ITR
	|ITR E
	|E
	;
%%

void yyerror(char *s) {
	printf("%s\n", s);
}

int main() {
	yyparse();
	return 0;
}
